import 'package:dart_app_lab1/dart_app_lab1.dart' as dart_app_lab1;
import 'dart:io';

void main() {
  print('Enter your text for count words');
  String paragraph00 = (stdin.readLineSync()!);
  //String paragraph0 =
  //   paragraph00.replaceAll(RegExp('“|,|”|-|\\(|\\)|;|"|!|'), '');
  String paragraph0 = paragraph00.replaceAll(RegExp('[^A-Za-z0-9]'), ' ');
  var parg0 = paragraph0.split(' ');
  print('Most words have following : \n $parg0');
  String paragraph = paragraph0.toUpperCase();
  var parg = paragraph.split(' ');
  int countWords = parg.length;
  print('This paragraph have $countWords words');
  int countChars = paragraph.length;
  print('This paragraph have $countChars characters');
  var nDupWords = <String>{};
  nDupWords.addAll(parg);
  int countNDupWords = nDupWords.length;
  print('How many each word have use in there : ');
  int count = 0;
  String p;
  for (int i = 0; i < countNDupWords; i++) {
    p = nDupWords.elementAt(i);
    for (int j = 0; j < countWords; j++) {
      if (p.compareTo(parg[j]) == 0) {
        count++;
      }
    }
    print('$p : $count times');
    count = 0;
  }
  print('Each word by non duplicate have $countNDupWords words : \n$nDupWords');
}
//test "My name is Mukku. My favorites is Grean Tea."
/*test2
1- There was nothing so very remarkable in that; nor did Alice think it so very much out of the way to hear the Rabbit say to itself, “Oh dear! Oh dear! I shall be late!” (when she thought it over afterwards, it occurred to her that she ought to have wondered 
2 - at this, but at the time it all seemed quite natural); but when the Rabbit actually took a watch out of its waistcoat-pocket, and looked at it, and then hurried on, Alice started to her feet, for it flashed across her mind that she had never before seen 
3 - a rabbit with either a waistcoat-pocket, or a watch to take out of it, and burning with curiosity, she ran across the field after it, and fortunately was just in time to see it pop down a large rabbit-hole under the hedge.
*/
//test3 "My name is Mukku. My favorites is Grean Tea. That book is my the first novel book."