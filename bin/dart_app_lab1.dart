import 'package:dart_app_lab1/dart_app_lab1.dart' as dart_app_lab1;
import 'dart:io';

void main() {
  print('Menu');
  print('Select the choice you want to perform :');
  print('1. ADD');
  print('2. SUBSTRACT');
  print('3. MULTIPLY');
  print('4. DIVIDE');
  print('5. EXIT');
  print('Choice you want to enter :');
  int? choice = int.parse(stdin.readLineSync()!);

  if (choice != 5) {
    print('Enter the value for x :');
    int? x = int.parse(stdin.readLineSync()!);
    print('Enter the value for y :');
    int? y = int.parse(stdin.readLineSync()!);
    int result = 0;

    if (choice == 1) {
      result = x + y;
      print('Sum of the two numbers is \n $result');
    } else if (choice == 2) {
      result = x - y;
      print('Substract of the two numbers is \n $result');
    } else if (choice == 3) {
      result = x * y;
      print('Multiply of the two numbers is \n $result');
    } else if (choice == 4) {
      result = x ~/ y;
      print('Divide of the two numbers is \n $result');
    }
  } else {
    print('You are Exit! Bye Bye!');
  }
}
